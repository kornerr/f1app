//
//  AppDelegate.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/25/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window; //created by xcode
@end
