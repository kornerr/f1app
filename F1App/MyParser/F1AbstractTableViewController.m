//
//  F1AbstractTableViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/30/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "F1AbstractTableViewController.h"

@interface F1AbstractTableViewController ()

@end

@implementation F1AbstractTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self->tableCellHeight = 78;
    // REVIEW Использовать размер из XIB.
    self->tableViewCellReuseID = [[NSString alloc]
                                  initWithString:@"CustomCellReuseID"];
    // REVIEW Почему не просто присвоить = @"..."?
    self->tableViewNumberOfSections = 1;
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:
                                              NSLocalizedString(@"BACK_BUTTION_ITEM_TITLE",Nil)
                                              style:UIBarButtonItemStyleBordered
                                              target:self
                                              action:@selector(popViewControllerAnimated:)];
    [self.refreshControl addTarget:self
                         action:@selector(updateDataWithRefreshControl)
                         forControlEvents:UIControlEventValueChanged];
    [self.refreshControl beginRefreshing];

}
-(void)viewDidAppear:(BOOL)animated
{
    if (self->tableDataArray.count==0)
    // REVIEW Зачем, если во viewDidLoad есть beginRefreshing?
    {
        [self updateDataWithRefreshControl];
    }
}
-(void)updateDataWithRefreshControl
{
    [self->tableDataArray release];
    self->tableDataArray = nil;
    // REVIEW Зачем nil?
    [self->tableDataParser newDownloadedData:&self->tableDataArray];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self->tableViewNumberOfSections;
    // REVIEW Почему не вставить константу 1 сюда?
    // REVIEW Зачем нужна переменная?
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self->tableDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
                        cellForRowAtIndexPath:(NSIndexPath *)indexPath
                        // REVIEW Странные отступы. Xcode делает иначе.
{
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:self->tableViewCellReuseID
                             forIndexPath:indexPath];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView
                        heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return self->tableCellHeight;
}
-(void)dealloc
{
    [self.navigationItem.backBarButtonItem release];
    [self->tableViewCellReuseID release];
    [self.refreshControl release];
    [self->tableDataArray release];
    [super dealloc];
}
@end
