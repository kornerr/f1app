//
//  F1ImageDownloader.m
//  F1App
//
//  Created by Anikeev Dmitry on 8/11/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "F1ImageDownloader.h"

@implementation F1ImageDownloader
-(void)downloadImage
{
    
}
-(id)initWithTableView:(UITableView *)tableView
{
    if (self=[super init])
    {
        self->_tableView = tableView;
    }
    return self;
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self increaseCounter];
    [connection release];
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response
{
    
    if (response.statusCode!=200)
    {
        [self increaseCounter];
        [connection cancel];
        [connection release];
    }
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
        [self increaseCounter];
    NSLog(@"%@",[error localizedDescription]);
    // REVIEW Нет пробела после запятой.
    [connection release];
}
-(void)increaseCounter
{
    self->finishedRequestsCount++;
    if (self->finishedRequestsCount==self->_urlArray.count)
        // REVIEW Нет пробелов по краям ==.
    {
                [self->_tableView reloadData];
                // REVIEW Странный отступ. А также чрезвычайно некрасивый
                // REVIEW способ обновлять таблицу.
        [self->_urlArray removeAllObjects];

    }
}
@end
