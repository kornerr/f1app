//
//  F1AbstractMoreViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/30/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "F1AbstractMoreViewController.h"

@interface F1AbstractMoreViewController ()

@end

@implementation F1AbstractMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:NSLocalizedString(@"CONTROLLER_MENU_TITLE",Nil)
                                              // REVIEW Разве не nil?
                                              style:UIBarButtonItemStyleBordered
                                              target:self
                                              action:@selector(popToRootViewController)];
    // REVIEW 1. Нужен ли release/autorelease? Почему?
    // REVIEW Как связаны имя метода и дальнейшее
    // REVIEW использование/неиспользование release/autorelease?
    // REVIEW 2. В каком ещё методе можно создать и
    // REVIEW задать rightBarButtonItem?
}
-(void)popToRootViewController
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)setDataFromSelectedCell:(id) data
{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    [self.navigationItem.rightBarButtonItem release];
    [super dealloc];
}
@end
