//
//  F1AbstractMoreViewController.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/30/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  Abstract class for modal view controllers
 */
@interface F1AbstractMoreViewController : UIViewController
{
// REVIEW Зачем { } ?
}
-(void)setDataFromSelectedCell:(id) data;
-(void)popToRootViewController;
@end
