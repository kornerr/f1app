//
//  F1ImageDownloader.h
//  F1App
//
//  Created by Anikeev Dmitry on 8/11/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

// REVIEW Можно ли всю эту загрузку изображений
// REVIEW заменить функционалом UIImageView AFNetworking?

#import <Foundation/Foundation.h>
/**
 *  abstract class for image downloader
 */
@interface F1ImageDownloader : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
{
    @protected
    /**
     *  (weak) used for reload data in table view after data is parsed     
     */
    UITableView *_tableView;
    /**
     *  counter of finished requests
     */
    int finishedRequestsCount;
}
-(id)initWithTableView:(UITableView *)tableView;
/**
 *  array with requests
 */
@property (assign, nonatomic) NSMutableArray *urlArray;
// REVIEW @property лучше писать ДО методов. И отделять пустыми строками.
// REVIEW В куче сложно читать.
-(void)downloadImage;
@end
