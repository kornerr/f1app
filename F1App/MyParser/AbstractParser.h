//
//  AbstractParser.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/27/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTMLParser.h"
#import "F1ImageDownloader.h"
/**
 *  Abstract parser class
 */
@interface AbstractParser : NSObject <NSURLConnectionDelegate,
                                      NSURLConnectionDataDelegate>
{
    @protected
    /**
     *  timeout interval for NSURLRequest
     */
    CGFloat timeoutInterval;
    /**
     *  property stores data downloaded by NSURLConnection
     */
    NSMutableData *downloadedData;
    /**
     *  property stores link to html page for parsing
     */
    NSString *urlString;
    /**
     *  property stores parsed data
     */
    NSMutableArray *resultArray;
    /**
     *  (weak) used for reload data in table view after data is parsed
     */
    UITableView *_tableView;
    /**
     *  (weak) used for end refreshing in target view controller after
     data is parsed
     */
    UIRefreshControl *_refreshControl;
    NSArray **refToArrayWithData;
    /**
     *  array for request
     */
    NSMutableArray *requestImgArray;
    
}
-(id)initWithTableView:(UITableView *)tableView
     andRefreshControl:(UIRefreshControl *) refreshControl;
/**
 *  download and parse data
 *
 *  @param targetArray reference to array used to store data in view controller
 */
-(void)newDownloadedData:(NSArray **) targetArray;
// REVIEW Почему не назвать downloadData, раз это загрузка?
@end
