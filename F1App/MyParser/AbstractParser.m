//
//  AbstractParser.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/27/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//  REVIEW Почему Goodline?
//

#import "AbstractParser.h"
@implementation AbstractParser
-(id)initWithTableView:(UITableView *)tableView
     andRefreshControl:(UIRefreshControl *)refreshControl
{
    if (self=[super init])
    {
        self->_tableView = tableView;
        self->_refreshControl = refreshControl;
        self->downloadedData = [[NSMutableData alloc] init];
        self->resultArray = [[NSMutableArray alloc] init];
        self->requestImgArray = [[NSMutableArray alloc] init];
        self->timeoutInterval = 15.0;
    }
    return self;
}
-(void)newDownloadedData:(NSArray **) targetArray
{
    self->refToArrayWithData = targetArray;
    NSURLRequest *mainRequest = [NSURLRequest
                                 requestWithURL:[NSURL URLWithString:self->urlString]
                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                 timeoutInterval:self->timeoutInterval];
    NSURLConnection *mainConnection = [[NSURLConnection alloc]
                                       initWithRequest:mainRequest
                                       delegate:self
                                       startImmediately:NO];
    [mainConnection start];
}
-(void)connection:(NSURLConnection *)connection
        didReceiveResponse:(NSHTTPURLResponse *)response
{
    if (response.statusCode!=200)
    {
        [connection cancel];
        [connection release];
        [self->_refreshControl endRefreshing];
    }
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
        [connection release];
        [self->_refreshControl endRefreshing];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [downloadedData appendData:data];
}
- (void)dealloc
{
    [self->requestImgArray release];
    [self->downloadedData release];
    [self->resultArray release];
    [super dealloc];
}
@end
