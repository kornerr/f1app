//
//  F1AbstractTableViewController.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/30/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractParser.h"
#import "F1AbstractMoreViewController.h"
/**
 *  Abstract class for menu items table view controllers
 */
@interface F1AbstractTableViewController : UITableViewController
{
    @protected
    /**
     *  stores data for table
     */
    NSArray                         *tableDataArray;
    AbstractParser                  *tableDataParser;
    /**
     *  modal view controller for data from cell
     */
    F1AbstractMoreViewController    *tableMoreViewController;
    CGFloat                         tableCellHeight;
    int                             tableViewNumberOfSections;
    NSString                        *tableViewCellReuseID;
    /**
     *  xib name with modal view controller interface
     */
    NSString                        *tableMoreViewControllerNibName;
    /**
     *  xib name with cell interface
     */
    NSString                        *tableViewCellNibName;
    
}
/**
 *  method for refresh controll
 */
-(void)updateDataWithRefreshControl;
@end
