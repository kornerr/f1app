//
//  DriverStandingsViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriverStandingsViewController.h"

@interface DriverStandingsViewController ()

@end

@implementation DriverStandingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self->tableMoreViewControllerNibName =[[NSString alloc]
                                           initWithString:NSStringFromClass([DriverStandingsMoreViewController class])];
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"DriverStandingsCell"
                                 bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:self->tableViewCellReuseID];
    self->tableDataParser = [[DriverStandingsParser alloc]
                             initWithTableView:self.tableView
                             andRefreshControl:self.refreshControl];
    self->tableMoreViewController = [[DriverStandingsMoreViewController alloc]
                                     initWithNibName:self->tableMoreViewControllerNibName
                                     bundle:nil];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DriverStandingsCell *cell = [tableView dequeueReusableCellWithIdentifier:self->tableViewCellReuseID];
    if (cell == nil) {
        cell = [[[DriverStandingsCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:self->tableViewCellReuseID]
                                             autorelease];
    }
    DriverStandings *driver = [self->tableDataArray objectAtIndex:indexPath.row];
    [[cell driver] setText:driver.driver];
    [[cell driverCountryImage] setImage:driver.driverCountryImage];
    [[cell driverPoints] setText:[NSString stringWithFormat:@"%i",driver.driverPoints]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self->tableMoreViewController
        setDataFromSelectedCell:[self->tableDataArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:self->tableMoreViewController animated:YES];
}
-(void)dealloc
{
    [self->tableMoreViewControllerNibName release];
    [self->tableDataParser release];
    [self->tableMoreViewController release];
    [super dealloc];
}
@end
