//
//  DriverStandingsCell.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriverStandingsCell : UITableViewCell
@property (assign, nonatomic) IBOutlet UIImageView *driverCountryImage;
@property (assign, nonatomic) IBOutlet UILabel *driver;
@property (assign, nonatomic) IBOutlet UILabel *driverPoints;
@end
