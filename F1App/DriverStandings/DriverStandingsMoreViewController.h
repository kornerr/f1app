//
//  DriverStandingsMoreViewController.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DriverStandings.h"
#import "F1AbstractMoreViewController.h"
@interface DriverStandingsMoreViewController : F1AbstractMoreViewController
{
    @private
    DriverStandings *selectedDriver;
}
@property (assign, nonatomic) IBOutlet UIImageView *driverImage;
@property (assign, nonatomic) IBOutlet UILabel *driverPositionLabel;
@property (assign, nonatomic) IBOutlet UILabel *driverWinsLabel;
@property (assign, nonatomic) IBOutlet UILabel *driverPointsLabel;
@property (assign, nonatomic) IBOutlet UILabel *driverTeamLabel;
@end
