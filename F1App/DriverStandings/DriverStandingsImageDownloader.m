//
//  DriverStandingsImageDownloader.m
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriverStandingsImageDownloader.h"

@implementation DriverStandingsImageDownloader
-(void)downloadImage
{
    self->finishedRequestsCount = 0;
    NSURLConnection *connect;
    for (int i=0;i<self.urlArray.count;i++)
    {
        DriverStandingsCustomRequest *curReq = [self.urlArray objectAtIndex:i];
        connect = [[NSURLConnection alloc] initWithRequest:curReq delegate:self startImmediately:YES];
        
    }
    
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    @autoreleasepool {
        
        DriverStandingsCustomRequest *curReq = (DriverStandingsCustomRequest*)connection.currentRequest;
        [curReq.selectedDriverStandings setDriverCountryImage:[UIImage imageWithData:data]];
    }
}
@end
