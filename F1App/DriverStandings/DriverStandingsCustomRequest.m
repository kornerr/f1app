//
//  DriverStandingsCustomRequest.m
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriverStandingsCustomRequest.h"

@implementation DriverStandingsCustomRequest
- (void)dealloc
{
    [self.selectedDriverStandings release];
    [super dealloc];
}
@end
