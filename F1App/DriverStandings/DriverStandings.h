//
//  DriverStandings.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DriverStandings : NSObject
@property (retain, nonatomic) NSString *driver;
@property (retain, nonatomic) NSString *driverTeam;
@property (retain, nonatomic) UIImage *driverCountryImage;
@property int driverPoints;
@property int driverPosition;
@property int driverWins;
@end
