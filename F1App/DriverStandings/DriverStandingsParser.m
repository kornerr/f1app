//
//  DriverStandingsParser.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriverStandingsParser.h"

@implementation DriverStandingsParser
-(id)initWithTableView:(UITableView *)tableView andRefreshControl:(UIRefreshControl *)refreshControl
{
    if (self=[super initWithTableView:tableView andRefreshControl:refreshControl])
    {
        self->urlString = [[NSString alloc]
                           initWithString:@"http://f1only.ru/season2014/lichnyj-zachet"];
        self->imgDownloader = [[DriverStandingsImageDownloader alloc] initWithTableView:self->_tableView];
        [self->imgDownloader setUrlArray:self->requestImgArray];
    }
    return self;
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    @autoreleasepool {
        NSString *res = [[NSString alloc] initWithData:downloadedData encoding:NSUTF8StringEncoding];
        HTMLParser *parser = [[HTMLParser alloc]
                              initWithString:res error:nil];
        HTMLNode *bodyNode = [parser body];
        
        NSArray *inputNodes = [[[bodyNode
                                findChildWithAttribute:@"id"
                                matchingName:@"tracks"
                                allowPartial:YES]
                                findChildTag:@"tbody"]
                                findChildTags:@"tr"];
        
        //tr - 0, 2, 3, 4, 5, 6
        for (int i=0;i<inputNodes.count;i++)
        {
            HTMLNode *curNode = [inputNodes objectAtIndex:i];
            NSArray *tags = [curNode findChildTags:@"td"];
            int position = [[[tags objectAtIndex:0] allContents] intValue];
            NSString *urlForImage = [[[tags objectAtIndex:2]
                                      findChildTag:@"img"]
                                      getAttributeNamed:@"src"];
            /*
             // works faster than code below
//            NSCharacterSet *charc=[NSCharacterSet characterSetWithCharactersInString:@" " ];
//            NSString *driver = [[[[tags objectAtIndex:3] allContents] stringByReplacingOccurrencesOfString:@"\n" withString:@""]stringByTrimmingCharactersInSet:charc];
//            
//            NSString *team = [[[[tags objectAtIndex:4] allContents] stringByReplacingOccurrencesOfString:@"\n" withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//            int points = [[[tags objectAtIndex:5] allContents] intValue];
            /*/
            
            
            NSString *driver = [[[tags objectAtIndex:3] allContents]
                                stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *team = [[[tags objectAtIndex:4] allContents]
                              stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            int points = [[[tags objectAtIndex:5] allContents] intValue];
            
            
            int wins = [[[tags objectAtIndex:6] allContents] intValue];
            
            DriverStandings *drStandings = [[DriverStandings alloc] init];
            [drStandings setDriver:driver];
            [drStandings setDriverPoints:points];
            [drStandings setDriverPosition:position];
            [drStandings setDriverTeam:team];
            [drStandings setDriverWins:wins];
            [self->resultArray addObject:drStandings];
            
            DriverStandingsCustomRequest *curReq = [[DriverStandingsCustomRequest alloc]
                                                    initWithURL:[NSURL URLWithString:urlForImage]
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                    timeoutInterval:self->timeoutInterval];
            [curReq setSelectedDriverStandings:drStandings];
            [requestImgArray addObject:curReq];
            [curReq release];
            
            [drStandings release];

        }
        [self->imgDownloader downloadImage];
        [parser release];
        *self->refToArrayWithData = [[NSArray arrayWithArray:self->resultArray] copy];
        [res release];
        [connection release];
        [self->downloadedData setLength:0];
        [self->resultArray removeAllObjects];
        [self->_refreshControl endRefreshing];
        [self->_tableView reloadData];

    }
}
-(void)dealloc
{
    [self->imgDownloader release];
    [self->urlString release];
    [self->resultArray release];
    [super dealloc];
}
@end
