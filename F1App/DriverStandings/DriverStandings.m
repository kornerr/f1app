//
//  DriverStandings.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriverStandings.h"

@implementation DriverStandings
-(void)dealloc
{
    [self.driver release];
    [self.driverCountryImage release];
    [self.driverTeam release];
    [super dealloc];
}
@end
