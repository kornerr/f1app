//
//  DriverStandingsParser.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "AbstractParser.h"
#import "DriverStandings.h"
#import "DriverStandingsImageDownloader.h"
@interface DriverStandingsParser : AbstractParser
{
    @protected
    DriverStandingsImageDownloader *imgDownloader;
}
@end
