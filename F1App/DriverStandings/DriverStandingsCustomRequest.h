//
//  DriverStandingsCustomRequest.h
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DriverStandings.h"
@interface DriverStandingsCustomRequest : NSURLRequest
@property (retain, nonatomic) DriverStandings *selectedDriverStandings;
@end
