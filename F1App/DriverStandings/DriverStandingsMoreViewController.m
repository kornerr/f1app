//
//  DriverStandingsMoreViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriverStandingsMoreViewController.h"

@interface DriverStandingsMoreViewController ()

@end

@implementation DriverStandingsMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{

        [self.driverPositionLabel setText:[NSString stringWithFormat:@"%i", self->selectedDriver.driverPosition]];
        [self.driverPointsLabel setText:[NSString stringWithFormat:@"%i", self->selectedDriver.driverPoints]];
        [self.driverTeamLabel setText:self->selectedDriver.driverTeam];
        [self.driverImage setImage:self->selectedDriver.driverCountryImage];
        [self.driverWinsLabel setText:[NSString stringWithFormat:@"%i", self->selectedDriver.driverWins]];
        [self.navigationItem setTitle:self->selectedDriver.driver];
    
}
-(void)setDataFromSelectedCell:(DriverStandings *) data
{
    self->selectedDriver = data;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
