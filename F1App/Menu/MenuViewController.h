//
//  MenuViewController.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/25/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CalendarViewController.h"
#import "DriverStandingsViewController.h"
#import "TeamStandingsViewController.h"
#import "DriversInfoViewController.h"
@interface MenuViewController : UITableViewController
{
    @protected
    /**
     *  store titles of view controllers
     */
    NSArray *menuItemsTitles;
    /**
     *  store instances of view controllers
     */
    NSArray *menuItemsViewControllers;
    int numbersOfSections;
}
@end
