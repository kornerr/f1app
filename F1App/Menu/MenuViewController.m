//
//  MenuViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/25/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"CONTROLLER_MENU_TITLE",Nil);
    self->menuItemsTitles = [[NSArray alloc]
                             initWithObjects:@"Календарь",
                                             @"Личный зачет",
                                             @"Кубок конструкторов",
                                             @"Пилоты", nil];
    // REVIEW Русские названия должны браться лишь из переводов.
    self->menuItemsViewControllers = [[NSArray alloc] initWithObjects:
                                 [CalendarViewController alloc],
                                 [DriverStandingsViewController alloc],
                                 [TeamStandingsViewController alloc],
                                 [DriversInfoViewController alloc],
                                 nil];
    // REVIEW Гораздо лучше сделать метод registerViewController или что-то
    // REVIEW такое, т.к. по-хорошему знать меню обо всех контроллерах
    // REVIEW совсем не нужно. Всё обо всех должен знать лишь AppDelegate.
    
    for (int i=0;i<self->menuItemsTitles.count;i++)
    {
        UIViewController *curViewController =
                                [self->menuItemsViewControllers objectAtIndex:i];
        [curViewController setTitle:[self->menuItemsTitles objectAtIndex:i]];
        [curViewController initWithNibName:
                                NSStringFromClass([[self->menuItemsViewControllers objectAtIndex:i] class])
                                bundle:nil];
    }
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithTitle:NSLocalizedString(@"BACK_BUTTION_ITEM_TITLE",Nil)
                                             style:UIBarButtonItemStyleBordered
                                             target:self
                                             action:@selector(popViewControllerAnimated:)];
    self->numbersOfSections = 1;
    
}
- (void)refresh:(UIRefreshControl *)refreshControl {
    [refreshControl endRefreshing];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self->numbersOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self->menuItemsTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    [cell.textLabel setText:[self->menuItemsTitles objectAtIndex:indexPath.row]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.navigationController pushViewController:[self->menuItemsViewControllers objectAtIndex:indexPath.row]  animated:YES];
}
 
-(void)dealloc
{
    [self.navigationItem.backBarButtonItem release];
    [self->menuItemsTitles release];
    [self->menuItemsViewControllers release];
    [super dealloc];
}

@end
