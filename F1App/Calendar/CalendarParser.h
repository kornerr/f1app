//
//  CalendarParser.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/27/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "AbstractParser.h"
#import "CalendarRace.h"
#import "CalendarImageDownloader.h"
@interface CalendarParser : AbstractParser
{
    @protected
    CalendarImageDownloader *imgDownloader;
    
}
@end
