//
//  CalendarViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/25/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "CalendarViewController.h"

@interface CalendarViewController ()

@end

@implementation CalendarViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self->tableMoreViewControllerNibName =[[NSString alloc]
                                           initWithString:NSStringFromClass([CalendarMoreViewController class])];
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"CalendarCell"
                                 bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:self->tableViewCellReuseID];
    self->tableDataParser = [[CalendarParser alloc]
                             initWithTableView:self.tableView
                             andRefreshControl:self.refreshControl];
    self->tableMoreViewController = [[CalendarMoreViewController alloc]
                                     initWithNibName:self->tableMoreViewControllerNibName
                                     bundle:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CalendarCell *cell = [tableView dequeueReusableCellWithIdentifier:self->tableViewCellReuseID];
    if (cell == nil) {
        cell = [[[CalendarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:self->tableViewCellReuseID] autorelease];
        // REVIEW Так не бывает. Как работает dequeue?
    }
    CalendarRace *curRace = [self->tableDataArray objectAtIndex:indexPath.row];
    [[cell calendarCountryLabel] setText:[curRace raceCountry]];
    [[cell calendarDateLabel] setText:[curRace raceDate]];
    // REVIEW Почему не cell.calendarDateLabel?
    [[cell calendarImageView] setImage:[curRace raceImage]];
    [[cell calendatTrackLabel] setText:[curRace raceTrack]];
    // REVIEW Гораздо лучше было вы сделать у CalendarCell какой-нибудь
    // REVIEW метод, куда можно было бы передать данные, а он бы уже
    // REVIEW всё выставил. И аутлеты были бы приватные.
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self->tableMoreViewController setDataFromSelectedCell:[self->tableDataArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:self->tableMoreViewController animated:YES];
}
-(void)dealloc
{
    [self->tableMoreViewControllerNibName release];
    [self->tableDataParser release];
    [self->tableMoreViewController release];
    [super dealloc];
}
@end
