//
//  CalendarMoreViewController.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/27/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarRace.h"
#import "F1AbstractMoreViewController.h"
@interface CalendarMoreViewController : F1AbstractMoreViewController
{
    @private
    CalendarRace *selectedRace;
}
@property (assign, nonatomic) IBOutlet UILabel *raceDate;
@property (assign, nonatomic) IBOutlet UILabel *raceCountry;
@property (assign, nonatomic) IBOutlet UILabel *raceTrack;
@property (assign, nonatomic) IBOutlet UILabel *raceLapCount;
@property (assign, nonatomic) IBOutlet UILabel *raceDistance;
@property (assign, nonatomic) IBOutlet UIImageView *raceImage;
// REVIEW Приватные аутлеты стоит размещать
// REVIEW в приватной категории.
@end
