//
//  CalendarImageDownloader.m
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "CalendarImageDownloader.h"

@implementation CalendarImageDownloader
-(void)downloadImage
{
    self->finishedRequestsCount = 0;
    NSURLConnection *connect;
    for (int i=0;i<self.urlArray.count;i++)
    {
        CalendarCustomRequest *curReq = [self.urlArray objectAtIndex:i];
        connect = [[NSURLConnection alloc] initWithRequest:curReq delegate:self
                                           startImmediately:YES];
        
    }
    
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    @autoreleasepool {
        // REVIEW Зачем autoreleasepool?
        CalendarCustomRequest *curReq = (CalendarCustomRequest*)connection.currentRequest;
        [curReq.selectedRace setRaceImage:[UIImage imageWithData:data]];
    }
}
@end
