//
//  CalendarCell.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/25/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarCell : UITableViewCell
@property (assign, nonatomic) IBOutlet UIImageView *calendarImageView;
@property (assign, nonatomic) IBOutlet UILabel *calendarDateLabel;
@property (assign, nonatomic) IBOutlet UILabel *calendarCountryLabel;
@property (assign, nonatomic) IBOutlet UILabel *calendatTrackLabel;
@end
