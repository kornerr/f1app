//
//  CalendarMoreViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/27/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "CalendarMoreViewController.h"

@interface CalendarMoreViewController ()

@end

@implementation CalendarMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.raceCountry setText:selectedRace.raceCountry];
    [self.raceDate setText:selectedRace.raceDate];
    [self.raceDistance setText:selectedRace.raceDistance];
    [self.raceImage setImage:selectedRace.raceImage];
    [self.raceLapCount setText:[NSString stringWithFormat:@"%i", selectedRace.raceLapCount]];
    [self.raceTrack setText:selectedRace.raceTrack];
    [self.navigationItem setTitle:selectedRace.raceCountry];
}
-(void)setDataFromSelectedCell:(id)data
{
    self->selectedRace = data;
    // REVIEW Если известен тип данных, почему id?



}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
