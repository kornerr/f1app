//
//  CalendarCustomRequest.h
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CalendarRace.h"
@interface CalendarCustomRequest : NSURLRequest
@property (retain, nonatomic) CalendarRace *selectedRace;
// REVIEW Зачем вполне абстратному запросу
// REVIEW иметь у себя детали реализации?
// REVIEW Беда с отсутствием пустых строк. Надо
// REVIEW всё-таки пустые строки делать.
@end
