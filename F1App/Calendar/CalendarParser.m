//
//  CalendarParser.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/27/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "CalendarParser.h"
@implementation CalendarParser
-(id)initWithTableView:(UITableView *)tableView andRefreshControl:(UIRefreshControl *)refreshControl
{
    if (self=[super initWithTableView:tableView andRefreshControl:refreshControl])
    {
        self->urlString = [[NSString alloc]
                           initWithString:@"http://www.f-1.ru/championship/2014-presentations/2014-calendar/"];
        self->imgDownloader = [[CalendarImageDownloader alloc]
                               initWithTableView:self->_tableView];
        [self->imgDownloader setUrlArray:requestImgArray];
        
    }
    return self;
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    @autoreleasepool {
        
        NSString *res = [[NSString alloc]
                         initWithData:downloadedData
                         encoding:NSUTF8StringEncoding];
        HTMLParser *parser = [[HTMLParser alloc]
                              initWithString:res error:nil];
        HTMLNode *bodyNode = [parser body];
        
        //should skip first row. its used as table head
        NSArray *inputNodes = [[bodyNode
                                findChildOfClass:@"track_description"]
                               findChildTags:@"tr"];
        
        for (int i=1;i<inputNodes.count;i++)
        {
            HTMLNode *curRow = [inputNodes objectAtIndex:i];
            NSArray *tags = [curRow findChildTags:@"p"];
            NSString *date = [[tags objectAtIndex:0] allContents];
            NSString *country = [[tags objectAtIndex:1] allContents];
            NSString *urlImage = [@"http:"
                                  stringByAppendingString:
                                  [[[tags objectAtIndex:1]
                                    findChildTag:@"img"]
                                   getAttributeNamed:@"src"]];
            // REVIEW Неебически большая вложенность.
            // REVIEW Разрывает мозг. Надо упрощать.
            NSString *track = [[tags objectAtIndex:2] allContents];
            int lapCount = [[[tags objectAtIndex:3] allContents] intValue];
            NSString *distance = [[tags objectAtIndex:4]  allContents];
            CalendarRace *race =[[CalendarRace alloc] init];
            [race setRaceCountry:country];
            [race setRaceDate:date];
            [race setRaceDistance:distance];
            [race setRaceLapCount:lapCount];
            [race setRaceTrack:track];
            
            CalendarCustomRequest *curReq = [[CalendarCustomRequest alloc]
                                             initWithURL:[NSURL URLWithString:urlImage]
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                             timeoutInterval:self->timeoutInterval];
            [curReq setSelectedRace:race];
            [requestImgArray addObject:curReq];
            [curReq release];
            [resultArray addObject:race];
            [race release];
        }
        
        [self->imgDownloader downloadImage];
        *self->refToArrayWithData = [[NSArray arrayWithArray:self->resultArray] copy];
        [parser release];
        [res release];
        [connection release];
        [self->downloadedData setLength:0];
        [self->resultArray removeAllObjects];
        [self->_refreshControl endRefreshing];
        [self->_tableView reloadData];
    }
}

-(void)dealloc
{
    [self->imgDownloader release];
    [self->urlString release];
    [super dealloc];
}
@end
