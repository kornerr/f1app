//
//  Race.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/26/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "CalendarRace.h"

@implementation CalendarRace
-(void)dealloc
{
    [self.raceCountry release];
    [self.raceDate release];
    [self.raceImage release];
    [self.raceTrack release];
    [self.raceDistance release];
    [super dealloc];
}
@end
