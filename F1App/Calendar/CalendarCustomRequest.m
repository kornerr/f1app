//
//  CalendarCustomRequest.m
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "CalendarCustomRequest.h"

@implementation CalendarCustomRequest
- (void)dealloc
{
    [self.selectedRace release];
    [super dealloc];
}
@end
