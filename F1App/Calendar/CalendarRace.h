//
//  Race.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/26/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarRace : NSObject
@property (retain, nonatomic) UIImage *raceImage;
@property (retain, nonatomic) NSString *raceDate;
@property (retain, nonatomic) NSString *raceCountry;
@property (retain, nonatomic) NSString *raceTrack;
@property int raceLapCount;
@property (retain, nonatomic) NSString *raceDistance;
@end
