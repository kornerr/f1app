//
//  DriversInfoViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/29/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriversInfoViewController.h"

@interface DriversInfoViewController ()

@end

@implementation DriversInfoViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self->tableMoreViewControllerNibName = [[NSString alloc]
                                            initWithString:NSStringFromClass([DriversInfoMoreViewController class])];
    [self.tableView registerNib:[UINib
                                    nibWithNibName:@"DriversInfoCell"
                                    bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:self->tableViewCellReuseID];
    self->tableDataParser = [[DriversInfoParser alloc]
                                    initWithTableView:self.tableView
                                    andRefreshControl:self.refreshControl];
    self->tableMoreViewController = [[DriversInfoMoreViewController alloc]
                                     initWithNibName:self->tableMoreViewControllerNibName
                                     bundle:nil];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DriversInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:self->tableViewCellReuseID];
    if (cell == nil) {
        cell = [[[DriversInfoCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:self->tableViewCellReuseID]
                                         autorelease];
    }
    DriversInfo *driverInfo = [self->tableDataArray objectAtIndex:indexPath.row];
    [[cell driverLabel] setText:driverInfo.driver];
    [[cell driverPhotoImage] setImage:driverInfo.driverPhotoImage];
    [[cell driverCountryImage] setImage:driverInfo.driverCountryImage];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self->tableMoreViewController
        setDataFromSelectedCell:[self->tableDataArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:self->tableMoreViewController animated:YES];
}
-(void)dealloc
{
    [self->tableMoreViewControllerNibName release];
    [self->tableDataParser release];
    [self->tableMoreViewController release];
    [super dealloc];
}
@end
