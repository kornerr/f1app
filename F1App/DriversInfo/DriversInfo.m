//
//  DriversInfo.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/29/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriversInfo.h"

@implementation DriversInfo
-(void)dealloc
{
    [self.driver release];
    [self.driverBirthday release];
    [self.driverCountryImage release];
    [self.driverPhotoImage release];
    [self.driverTeam release];
    [super dealloc];
}
@end
