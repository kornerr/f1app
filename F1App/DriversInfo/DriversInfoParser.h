//
//  DriversInfoParser.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/29/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "AbstractParser.h"
#import "DriversInfo.h"
#import "DriversInfoImageDownloader.h"
@interface DriversInfoParser : AbstractParser
{
    @protected
    DriversInfoImageDownloader *imgDownloader;
    /**
     *  addiditional instance for image downloader because DriversInfo class
     contains two UIImage property
     */
    DriversInfoImageDownloader *additionalImgDownloader;
    /**
     *  additional array for requests to images
     */
    NSMutableArray *additionalRequestImgArray;
}
@end
