//
//  DriversInfo.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/29/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DriversInfo : NSObject
@property (retain, nonatomic) NSString *driver;
@property (retain, nonatomic) UIImage *driverPhotoImage;
@property (retain, nonatomic) UIImage *driverCountryImage;
@property (retain, nonatomic) NSString *driverTeam;
@property (retain, nonatomic) NSString *driverBirthday;
@property int driverHeight;
@property int driverWeight;
@end
