//
//  DriversInfoMoreViewController.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/29/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DriversInfo.h"
#import "F1AbstractMoreViewController.h"
@interface DriversInfoMoreViewController : F1AbstractMoreViewController
{
    @private
    DriversInfo *selectedDriversInfo;
}
@property (assign, nonatomic) IBOutlet UIImageView *driverPhotoImage;
@property (assign, nonatomic) IBOutlet UIImageView *driverCountryImage;
@property (assign, nonatomic) IBOutlet UILabel *driverTeamLabel;
@property (assign, nonatomic) IBOutlet UILabel *driverBirthday;
@property (assign, nonatomic) IBOutlet UILabel *driverHeight;
@property (assign, nonatomic) IBOutlet UILabel *driverWeight;
@end
