//
//  DriversInfoImageDownloader.h
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "F1ImageDownloader.h"
#import "DriversInfoCustomRequest.h"
@interface DriversInfoImageDownloader : F1ImageDownloader
{
    @public
    bool isFirstImage;
}
@end
