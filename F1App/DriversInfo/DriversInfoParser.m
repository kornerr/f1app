//
//  DriversInfoParser.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/29/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriversInfoParser.h"

@implementation DriversInfoParser
-(id)initWithTableView:(UITableView *)tableView andRefreshControl:(UIRefreshControl *)refreshControl
{
    if (self=[super initWithTableView:tableView andRefreshControl:refreshControl])
    {
        self->urlString = [[NSString alloc]
                           initWithString:@"http://www.championat.com/auto/_f1/247/astat_players.html"];
        self->imgDownloader = [[DriversInfoImageDownloader alloc]
                               initWithTableView:self->_tableView];
        //sets main downloader as first image
        self->imgDownloader->isFirstImage = YES;
        [self->imgDownloader setUrlArray:requestImgArray];
        self->additionalImgDownloader =[[DriversInfoImageDownloader alloc]
                                        initWithTableView:self->_tableView];
        //sets additional downloader as second image
        self->additionalImgDownloader->isFirstImage=NO;
        self->additionalRequestImgArray = [[NSMutableArray alloc] init];
        [self->additionalImgDownloader setUrlArray:additionalRequestImgArray];
    }
    return  self;
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    @autoreleasepool {
        NSString *res = [[NSString alloc]
                         initWithData:downloadedData
                         encoding:NSUTF8StringEncoding];
        HTMLParser *parser = [[HTMLParser alloc]
                              initWithString:res error:nil];
        HTMLNode *bodyNode = [parser body];
        NSArray *inputNodes = [[bodyNode
                                findChildWithAttribute:@"id"
                                matchingName:@"player-list" allowPartial:YES]
                                findChildTags:@"tr"];
        
        for (int i=0;i<inputNodes.count;i++)
        {
            HTMLNode *curNode = [inputNodes objectAtIndex:i];
            NSArray *rows = [curNode findChildTags:@"td"];
            NSString *urlCountryImage = [[[rows objectAtIndex:0]
                                          findChildTag:@"img"]
                                          getAttributeNamed:@"src"];
            NSString *urlPhotoImage =[[[rows objectAtIndex:1]
                                       findChildTag:@"img"]
                                       getAttributeNamed:@"src"];
            
            NSString *driver = [[[rows objectAtIndex:2] allContents]
                                stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString *team = [[rows objectAtIndex:3] allContents];
            NSString *birthday = [[rows objectAtIndex:4] allContents];
            NSString *height = [[rows objectAtIndex:5] allContents];
            NSString *weight = [[rows objectAtIndex:6] allContents];
            
            DriversInfo *driverInfo = [[DriversInfo alloc] init];
            [driverInfo setDriver:driver];
            [driverInfo setDriverBirthday:birthday];
            [driverInfo setDriverHeight:[height intValue]];
            [driverInfo setDriverTeam:team];
            [driverInfo setDriverWeight:[weight intValue]];
            
            DriversInfoCustomRequest *curReq = [[DriversInfoCustomRequest alloc]
                                                initWithURL:[NSURL URLWithString:urlCountryImage]
                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                timeoutInterval:self->timeoutInterval];
            [curReq setSelectedDriversInfo:driverInfo];
            [requestImgArray addObject:curReq];
            
            
            DriversInfoCustomRequest *curReq1 = [[DriversInfoCustomRequest alloc]
                                                 initWithURL:[NSURL URLWithString:urlPhotoImage]
                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                 timeoutInterval:self->timeoutInterval];
            [curReq1 setSelectedDriversInfo:driverInfo];
            [additionalRequestImgArray addObject:curReq1];
            
            
            
            [self->resultArray addObject:driverInfo];
            [curReq release];
            [curReq1 release];
            [driverInfo release];
            
        }
        [self->imgDownloader downloadImage];
        [self->additionalImgDownloader downloadImage];
        [parser release];
        *self->refToArrayWithData = [[NSArray alloc] initWithArray:self->resultArray];
        [res release];
        [connection release];
        [self->downloadedData setLength:0];
        [self->resultArray removeAllObjects];
        [self->_refreshControl endRefreshing];
        [self->_tableView reloadData];

    }
}
-(void)dealloc
{
    [self->imgDownloader release];
    [self->urlString release];
    [self->additionalRequestImgArray release];
    [self->additionalImgDownloader release];
    [super dealloc];
}
@end
