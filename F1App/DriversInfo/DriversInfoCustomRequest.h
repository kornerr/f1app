//
//  DriversInfoCustomRequest.h
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DriversInfo.h"
@interface DriversInfoCustomRequest : NSURLRequest
@property (retain, nonatomic) DriversInfo *selectedDriversInfo;
@end
