//
//  DriversInfoImageDownloader.m
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriversInfoImageDownloader.h"

@implementation DriversInfoImageDownloader
-(void)downloadImage
{
    self->finishedRequestsCount = 0;
    NSURLConnection *connect;
    for (int i=0;i<self.urlArray.count;i++)
    {
        DriversInfoCustomRequest *curReq = [self.urlArray objectAtIndex:i];
        connect = [[NSURLConnection alloc] initWithRequest:curReq delegate:self startImmediately:YES];
        
    }
    
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    @autoreleasepool {
        DriversInfoCustomRequest *curReq = (DriversInfoCustomRequest*)connection.currentRequest;
        if (isFirstImage)
            // REVIEW Всё время first image?
            [curReq.selectedDriversInfo setDriverCountryImage:[UIImage imageWithData:data]];
        else
            [curReq.selectedDriversInfo setDriverPhotoImage:[UIImage imageWithData:data]];
    }
}
@end
