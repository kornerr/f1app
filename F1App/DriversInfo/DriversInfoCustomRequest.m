//
//  DriversInfoCustomRequest.m
//  F1App
//
//  Created by Anikeev Dmitry on 8/12/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriversInfoCustomRequest.h"

@implementation DriversInfoCustomRequest
- (void)dealloc
{
    [self.selectedDriversInfo release];
    [super dealloc];
}
@end
