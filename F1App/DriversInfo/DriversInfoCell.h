//
//  DriversInfoCell.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/29/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriversInfoCell : UITableViewCell
@property (assign, nonatomic) IBOutlet UIImageView *driverPhotoImage;
@property (assign, nonatomic) IBOutlet UIImageView *driverCountryImage;
@property (assign, nonatomic) IBOutlet UILabel *driverLabel;
@end
