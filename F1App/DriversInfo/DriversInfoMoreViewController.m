//
//  DriversInfoMoreViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/29/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "DriversInfoMoreViewController.h"

@interface DriversInfoMoreViewController ()

@end

@implementation DriversInfoMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.driverCountryImage setImage:selectedDriversInfo.driverCountryImage];
    [self.driverPhotoImage setImage:selectedDriversInfo.driverPhotoImage];
    [self.driverHeight setText:[NSString stringWithFormat:@"%i",selectedDriversInfo.driverHeight]];
    [self.driverWeight setText:[NSString stringWithFormat:@"%i",selectedDriversInfo.driverWeight]];
    [self.driverTeamLabel setText:selectedDriversInfo.driverTeam];
    [self.driverBirthday setText:selectedDriversInfo.driverBirthday];
    [self.navigationItem setTitle:selectedDriversInfo.driver];
}
-(void)setDataFromSelectedCell:(DriversInfo *)data
{
    selectedDriversInfo = data;
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
