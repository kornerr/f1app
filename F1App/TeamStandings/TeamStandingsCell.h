//
//  TeamStandingsCell.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamStandingsCell : UITableViewCell
@property (assign, nonatomic) IBOutlet UIImageView *teamImage;
@property (assign, nonatomic) IBOutlet UILabel *team;
@property (assign, nonatomic) IBOutlet UILabel *teamPoints;
@end
