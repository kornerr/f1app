//
//  TeamStandings.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "TeamStandings.h"

@implementation TeamStandings
-(void)dealloc
{
    [self.team release];
    [self.teamImage release];
    [super dealloc];
}
@end
