//
//  TeamStandingsParser.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "AbstractParser.h"
#import "TeamStandings.h"
#import "TeamStandingsImageDownloader.h"
@interface TeamStandingsParser : AbstractParser
{
    @protected
    TeamStandingsImageDownloader *imgDownloader;
    /**
     *  additional link to page with data
     */
    NSString *secondUrlForImage;
}
@end
