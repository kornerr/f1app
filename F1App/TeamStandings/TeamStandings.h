//
//  TeamStandings.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamStandings : NSObject
@property (retain, nonatomic) NSString *team;
@property (retain, nonatomic) UIImage *teamImage;
@property int teamPoints;
@property int teamBestFinish;
@property int teamPosition;
@end
