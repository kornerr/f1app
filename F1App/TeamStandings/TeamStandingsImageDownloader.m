//
//  TeamStandingsImageDownloader.m
//  F1App
//
//  Created by Anikeev Dmitry on 8/13/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "TeamStandingsImageDownloader.h"
#import "HTMLParser.h"
@implementation TeamStandingsImageDownloader
-(void)downloadImage
{
    self->finishedRequestsCount = 0;

             NSURLConnection *connect;
             for (int i=0;i<self.urlArray.count;i++)
             {
                 TeamStandingsCustomRequest *curReq = [self.urlArray objectAtIndex:i];
                 connect = [[NSURLConnection alloc] initWithRequest:curReq delegate:self startImmediately:YES];
                 
             }
    
    
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    @autoreleasepool {
        TeamStandingsCustomRequest *curReq = (TeamStandingsCustomRequest*)connection.currentRequest;
        [curReq.selectedTeamStandings setTeamImage:[UIImage imageWithData:data]];
    }
}
@end
