//
//  TeamStandingsViewController.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamStandingsParser.h"
#import "TeamStandingsMoreViewController.h"
#import "TeamStandingsCell.h"
#import "TeamStandings.h"
#import "F1AbstractTableViewController.h"
@interface TeamStandingsViewController : F1AbstractTableViewController
@end
