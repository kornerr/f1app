//
//  TeamStandingsCustomRequest.h
//  F1App
//
//  Created by Anikeev Dmitry on 8/13/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TeamStandings.h"
@interface TeamStandingsCustomRequest : NSURLRequest
@property (retain, nonatomic) TeamStandings *selectedTeamStandings;
@end
