//
//  TeamStandingsMoreViewController.h
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamStandings.h"
#import "F1AbstractMoreViewController.h"
@interface TeamStandingsMoreViewController : F1AbstractMoreViewController{
    @private
    TeamStandings *selectedTeam;
}
@property (assign, nonatomic) IBOutlet UIImageView *teamImage;
@property (assign, nonatomic) IBOutlet UILabel *teamPoints;
@property (assign, nonatomic) IBOutlet UILabel *teamBestFinish;
@property (assign, nonatomic) IBOutlet UILabel *teamPosition;
@end
