//
//  TeamStandingsViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "TeamStandingsViewController.h"

@interface TeamStandingsViewController ()

@end

@implementation TeamStandingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self->tableMoreViewControllerNibName = [[NSString alloc]
                                            initWithString:NSStringFromClass([TeamStandingsMoreViewController class])];
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"TeamStandingsCell"
                                 bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:self->tableViewCellReuseID];
    self->tableDataParser = [[TeamStandingsParser alloc]
                             initWithTableView:self.tableView
                             andRefreshControl:self.refreshControl];

    self->tableMoreViewController = [[TeamStandingsMoreViewController alloc]
                                     initWithNibName:self->tableMoreViewControllerNibName bundle:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TeamStandingsCell *cell = [tableView
                               dequeueReusableCellWithIdentifier:self->tableViewCellReuseID];
    if (cell == nil) {
        cell = [[[TeamStandingsCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:self->tableViewCellReuseID] autorelease];
    }
    TeamStandings *team = [self->tableDataArray objectAtIndex:indexPath.row];
    [[cell team] setText:team.team];
    [[cell teamImage] setImage:team.teamImage];
    [[cell teamPoints] setText:[NSString stringWithFormat:@"%i",team.teamPoints]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self->tableMoreViewController setDataFromSelectedCell:[self->tableDataArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:self->tableMoreViewController animated:YES];
}
- (void)dealloc
{
    [self->tableMoreViewController release];
    [self->tableMoreViewControllerNibName release];
    [self->tableDataParser release];
    [super dealloc];
}
@end
