//
//  TeamStandingsCustomRequest.m
//  F1App
//
//  Created by Anikeev Dmitry on 8/13/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "TeamStandingsCustomRequest.h"

@implementation TeamStandingsCustomRequest
- (void)dealloc
{
    [self.selectedTeamStandings release];
    [super dealloc];
}
@end
