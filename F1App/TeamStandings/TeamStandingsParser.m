//
//  TeamStandingsParser.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "TeamStandingsParser.h"

@implementation TeamStandingsParser
-(id)initWithTableView:(UITableView *)tableView andRefreshControl:(UIRefreshControl *)refreshControl
{
    if (self=[super initWithTableView:tableView andRefreshControl:refreshControl])
    {
        self->urlString = [[NSString alloc]
                           initWithString:@"http://f1only.ru/season2014/komandnyj-zachet"];
        self->secondUrlForImage =[[NSString alloc]
                                  initWithString:@"http://www.liveresult.ru/formula-1/widget/standings/"];
        self->imgDownloader = [[TeamStandingsImageDownloader alloc]
                               initWithTableView:self->_tableView];
        [self->imgDownloader setUrlArray:requestImgArray];
    }
    return self;
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *res = [[NSString alloc] initWithData:downloadedData encoding:NSUTF8StringEncoding];
    HTMLParser *parser = [[HTMLParser alloc]
                         initWithString:res error:nil];

    @autoreleasepool {
    if ([[connection.currentRequest URL] isEqual:[NSURL URLWithString:self->urlString]])
    {
        HTMLNode *bodyNode = [parser body];
        NSArray *inputNodes = [[[[bodyNode
                                  findChildWithAttribute:@"id"
                                  matchingName:@"tracks"
                                  allowPartial:YES]
                                 findChildTag:@"tbody"]
                                findChildTags:@"tr"]
                               objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, 11)]];
        
        for (int i=0;i<inputNodes.count;i++)
        {
            HTMLNode *curNode = [inputNodes objectAtIndex:i];
            NSArray *tags = [curNode findChildTags:@"td"];
            int position = [[[tags objectAtIndex:0] allContents] intValue];
            NSString *team = [[[tags objectAtIndex:1] allContents] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            int points = [[[tags objectAtIndex:2] allContents] intValue];
            int best = [[[tags objectAtIndex:4] allContents] intValue];
            
            TeamStandings *teamStandings = [[TeamStandings alloc] init];
            [teamStandings setTeam:team];
            [teamStandings setTeamBestFinish:best];
            [teamStandings setTeamPoints:points];
            [teamStandings setTeamPosition:position];
            [self->resultArray addObject:teamStandings];
            [teamStandings release];
            
        }
        *self->refToArrayWithData = [[NSArray arrayWithArray:self->resultArray] copy];
        [self->resultArray removeAllObjects];
        [self->_refreshControl endRefreshing];
        [self->_tableView reloadData];
        NSURLRequest *secondReq = [[NSURLRequest alloc]
                                   initWithURL:[NSURL URLWithString:self->secondUrlForImage] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:self->timeoutInterval];
        NSURLConnection *secondConnection = [[NSURLConnection alloc]
                                             initWithRequest:secondReq
                                             delegate:self
                                             startImmediately:NO];
        [secondReq release];
        [secondConnection start];

    }
    else
    {
        HTMLNode *bodyNode = [parser body];
        NSArray *inputNodes =[[bodyNode
                               findChildWithAttribute:@"id"
                               matchingName:@"lmstandings_pane" allowPartial:YES]
                               findChildTags:@"tr"];
        NSArray *secondArrayNode = [inputNodes
                                    objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:
                                                      NSMakeRange(27, (*self->refToArrayWithData).count)]];
        //
        for (int i=0;i<secondArrayNode.count;i++)
        {
            HTMLNode *node = [secondArrayNode objectAtIndex:i];
            NSString *url = [[node findChildTag:@"img"] getAttributeNamed:@"src"];
            
            TeamStandingsCustomRequest *curReq = [[TeamStandingsCustomRequest alloc]
                                                  initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:self->timeoutInterval];
            [curReq setSelectedTeamStandings:[*self->refToArrayWithData objectAtIndex:i]];
            [self->requestImgArray addObject:curReq];
            [curReq release];
        }
        [self->imgDownloader downloadImage];

    }
    [self->downloadedData setLength:0];
    [parser release];
    [res release];
    [connection release];

    }
}
- (void)dealloc
{
    [self->imgDownloader release];
    [self->secondUrlForImage release];
    [self->urlString release];
    [super dealloc];
}
@end
