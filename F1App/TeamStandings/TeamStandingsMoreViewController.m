//
//  TeamStandingsMoreViewController.m
//  F1App
//
//  Created by Anikeev Dmitry on 7/28/14.
//  Copyright (c) 2014 Goodline. All rights reserved.
//

#import "TeamStandingsMoreViewController.h"

@interface TeamStandingsMoreViewController ()

@end

@implementation TeamStandingsMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated
{
        [self.teamImage setImage:[self->selectedTeam teamImage]];
        [self.teamBestFinish setText:[NSString stringWithFormat:@"%i",[self->selectedTeam teamBestFinish]]];
        [self.teamPoints setText:[NSString stringWithFormat:@"%i",[self->selectedTeam teamPoints]]];
        [self.teamPosition setText:[NSString stringWithFormat:@"%i",[self->selectedTeam teamPosition]]];
        [self.navigationItem setTitle:self->selectedTeam.team];
    
}
-(void)setDataFromSelectedCell:(TeamStandings *)data
{
    self->selectedTeam = data;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
